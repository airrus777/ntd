from pixellib.instance import instance_segmentation
import cv2

capture = cv2.VideoCapture(0)

segment_video = instance_segmentation(infer_speed='fast')
segment_video.load_model("mask_rcnn_coco.h5")
segment_video.process_camera(capture, frames_per_second=120,
                             output_video_name="output_video.mp4", show_frames=True,
                             show_bboxes=True,
                             frame_name="GOD_EYE")